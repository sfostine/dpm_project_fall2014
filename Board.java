/**
 * @author: Samuel Fostine
 * Group : 13
 * Date of Modification: 12-11-2014 
 * 
 * This Board class assign the coordinate for each tile and set if a tile is blocked or not by using the coordinate class
 */
package DPM_NXT_PROJECT;
 
public class Board {
      
    /** initialize 2D coordinate array, 2D Arrow array and 2D boolean visited array(this last one is for the ShortestPath class)
     */
    Coordinate tile[][];
    public  Arrow blocked [][];
    boolean visited[][];
    /**
     * size of the double arrays
     */
    int size;
 
    /** constructor for Board class
     * initialize the tiles
     * set the coordinates for each od the tiles
     * set if the tile is blocked or not
     * set if an arrow or direction is blocked by a blocked tile or not
     * @param size
     */
    public Board(int size, int [][] map) {
        this.size = size;
        // set the size of the 2D arrays
        tile = new Coordinate[size][size];
        blocked = new Arrow[size][size];
        visited = new boolean[size][size];
        // initialize x and y coordinates
        int x = -15, y = 75;
 
        /** passing through all tiles, set the tile with coordinates (15, -15), (45,45), (75, 45) and (-15, 75) to be blocked
         *at the same time it sets the coordinates for each tile
        *It also sets the arrows to false, that means at the first time, none of the arrows inside of the tile are blocked, using the 2D array blocked
        * later it will set some of the arrows to be blocked*/
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                // set all the arrow to be blocked
                blocked [i][j] = new Arrow(false, false, false, false);
                 
                // all the node from shortestPath class not to be visited
                visited[i][j] = false;
                // if the value at map[i][j] is 1, set the tile to be blocked
                if(map[i][j] == 1)
                    tile[i][j] = new Coordinate(x, y, true);
                else
                    tile[i][j] = new Coordinate(x, y, false);
                y -= 30;
            }
            y = 75;
            x += 30;
        }
         
        /** after setting all of the arrays not to be blocked,
         *  now we are blocking those that are supposed to be blocked from the starting*/
        for(int k = 0; k < size; k++)
        {
            for(int l = 0; l < size; l++)
            {
                // when i = 0, all of the arrows that are toward west will be blocked
                if(k == 0)
                {
                    blocked[k][l].setWest(true);
                }
                 
                // when i = size -1, all of the arrows that are toward East will be blocked
                if(k == (size-1))
                {
                    blocked[k][l].setEast(true);
                }
                 
                // when j = 0, all of the arrows that are toward North will be blocked
                if (l == 0)
                {
                    blocked[k][l].setNorth(true);
                }
                 
                // when j = size -1, all of the arrows that are toward west will be blocked
                if(l == (size-1))
                {
                    blocked[k][l].setSouth(true);
                }
                // if the tile is blocked set all the arrows of the tile to be blocked
                if(tile[k][l].isblocked())
                {
                    blocked[k][l].setNorth(true);
                    blocked[k][l].setSouth(true);
                    blocked[k][l].setWest(true);
                    blocked[k][l].setEast(true);
                }
                 
                // when a tile is blocked, all the arrows that are facing it are blocked
                if(tile[k][l].isblocked() && l < (size-1) && l >= 0)
                {
                    blocked[k][l+1].setNorth(true);
                 
                }
                 
                if(tile[k][l].isblocked() && l <= (size-1) && l > 0)
                {
                    blocked[k][l-1].setSouth(true);
                }
                 
                if( tile[k][l].isblocked() && k < (size-1) && k >= 0)
                {
                    blocked[k+1][l].setWest(true);
                }
                 
                if( tile[k][l].isblocked() && k <= (size-1) && k > 0)
                {
                    blocked[k-1][l].setEast(true);
                }
                 
            }
             
        }     
    }
    
    // reset the visited double array to it's initial values
    public void resetVisited()
    {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                // set all the arrow to be blocked
                visited[i][j] = false;
            }
        }
    }
     
    /** get the tile
    // with this method, we can retrieve information about each tile
     * 
     * @param int i
     * @param int j
     * @return tile[i][j]
     */
    public Coordinate getTile(int i, int j)
    {
        return tile[i][j];
    } 
}