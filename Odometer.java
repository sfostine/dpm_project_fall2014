/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 06-11-2014
 * 
 *The odometer class gives the right angle determines by the robot positions abd orientations
 */

package DPM_NXT_PROJECT;

import lejos.nxt.NXTRegulatedMotor;
import lejos.util.Timer;
import lejos.util.TimerListener;

public class Odometer implements TimerListener {
    public static final int DEFAULT_PERIOD = 25;
    private NXTRegulatedMotor leftMotor, rightMotor;
    private Timer odometerTimer;
    private Object lock;
    private double x, y, theta;
    private double [] oldDH, dDH;
      
    private double leftRadius, rightRadius, width;
      
    /**
     * Constructor for the odometer class
     * @param robot
     * @param period
     * @param start
     */
    public Odometer(TwoWheeledRobot robot, int period, boolean start) {
        odometerTimer = new Timer(period, this);
        leftMotor = robot.leftMotor;
        rightMotor = robot.rightMotor;
        this.rightRadius = robot.DEFAULT_RIGHT_RADIUS;
        this.leftRadius = robot.DEFAULT_LEFT_RADIUS;
        this.width = robot.DEFAULT_WIDTH;
        x = 0.0;
        y = 0.0;
        theta = 0.0;
        oldDH = new double [2];
        dDH = new double [2];
        lock = new Object();
        
        //start the timer
        if (start)
            odometerTimer.start();
    }
      
      /**
       * update the positions
       */
    public void timedOut() {    
         this.getVector(dDH);
        dDH[0] -= oldDH[0];
        dDH[1] -= oldDH[1];
  
        // update the position in a critical region
        synchronized (this) {
            theta -= dDH[1];
            theta = fixDegAngle(theta);
  
            x += dDH[0] * Math.cos(Math.toRadians(theta));
            y += dDH[0] * Math.sin(Math.toRadians(theta));
        }
  
        oldDH[0] += dDH[0];
        oldDH[1] += dDH[1]; 
    }
    
    /**Calculates the displacement and direction
     * 
     * @param  array of data
     */
    private void getVector(double[] data) {
        int leftTacho, rightTacho;
        leftTacho = leftMotor.getTachoCount();
        rightTacho = rightMotor.getTachoCount();
  
        data[0] = (leftTacho * leftRadius + rightTacho * rightRadius) * Math.PI / 360.0;
        data[1] = (rightTacho * rightRadius - leftTacho * leftRadius) / width;
    }
    
    /** Getters of x 
     * 
     * @return double x
     */
    public double getX() {
        synchronized (lock) {
            return x;
        }
    }
      /**
       * get Y
       * @return double y
       */
    public double getY() {
        synchronized (lock) {
            return y;
        }
    }
      
    /**
     * get the angle of the odometer
     * @return the angle double theta
     */
    public double getTheta() {
        synchronized (lock) {
            return theta;
        }
    }
    /**Setting the angle
     * 
     * @param double angle
     */
    public void setAng(double angle) {
        synchronized (lock) {
            theta = angle;
        }
    } 
    /**
     * set X
     * @param double X
     */
    public void setX(double X) {
        synchronized (lock) {
           this.x = X;
        }
    } 
    /**
     * set Y
     * @param double Y
     */
    public void setY(double Y) {
        synchronized (lock) {
           this.y = Y;
        }
    }
    /**
     * get the position of the robot
     * @param double array pos
     */
    public void getPosition(double [] pos) {
        synchronized (lock) {
            pos[0] = x;
            pos[1] = y;
            pos[2] = theta;
        }
    }      
    /** mutators
     * 
     * @param double array pos
     * @param double array update
     */
    public void setPosition(double [] pos, boolean [] update) {
        synchronized (lock) {
            if (update[0]) x = pos[0];
            if (update[1]) y = pos[1];
            if (update[2]) theta = pos[2];
        }
    }
  
    /** static helper methods to fix de angle of the odometer
     * 
     * @param double angle
     * @return the right angle
     */
    public static double fixDegAngle(double angle) {        
        if (angle < 0.0)
            angle = 360.0 + (angle % 360.0);
          
        return angle % 360.0;
    }
      /**
       * find the minimum angle
       * @param double angle a
       * @param double angle b
       * @return the min angle
       */
    public static double minimumAngleFromTo(double a, double b) {
        double d = fixDegAngle(b - a); 
        if (d < 180.0)
            return d;
        else
            return d - 360.0;
    }

} 
 
