/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 27-11-2014
 * 
 * Navigation class allows to navigate from a certain point to another one
 */
package DPM_NXT_PROJECT;


import lejos.nxt.NXTRegulatedMotor;

public class Navigation {
	TwoWheeledRobot robot;
	// some initilizations
	private final static int SLOW = 300;
	private final static double DEG_ERR = 3.0;
	private final double wheelRadius, width;
	private Odometer odometer;
	private NXTRegulatedMotor leftMotor, rightMotor;

	/** constructor for navigation
	 * 
	 * @param The Odometer odo
	 */
	public Navigation(Odometer odo, TwoWheeledRobot rob) {
		this.odometer = odo;
		this.robot = rob;
		wheelRadius = robot.getRightRadius();
		width = robot.getWidth();

		this.leftMotor = robot.leftMotor;
		this.rightMotor = robot.rightMotor;

		
	}

	/**
	 * Functions to set the motor speeds join
	 * @param double lSpd: left speed
	 * @param double rSpd:  right Speed
	 */
	public void setSpeeds(float lSpd, float rSpd) {
		this.leftMotor.setSpeed(Math.abs(lSpd));
		this.rightMotor.setSpeed(Math.abs(rSpd));
		if (lSpd < 0)
			this.leftMotor.backward();
		else
			this.leftMotor.forward();
		if (rSpd < 0)
			this.rightMotor.backward();
		else
			this.rightMotor.forward();
	}



	/**
	 *  TravelTo function which takes as arguments the x and y position in cm
	 * Will travel to designated position, while constantly updating it's heading
	 * @param double x
	 * @param double y
	 */
	public void travelTo(double x, double y, boolean bool) {
		int dist = 80;
		double dX, dY, angle, travelDis;
	

		// Get the x y and that we need to travel
		dX = x - odometer.getX();
		dY = y - odometer.getY();

		// find the direction by using tan
		angle = Math.atan2(dX, dY) * 180 / Math.PI;

		// Correct the angle until its good
			turnTo(angle, true);
		//}

		// Make the wheels move slowly
		leftMotor.setSpeed(SLOW);
		rightMotor.setSpeed(SLOW);

		// Calcuate the travel distance by using pythagorean
		travelDis = Math.sqrt(dX * dX + dY * dY + dist);

		// Move the robot a certain distance and then stop the motors
		leftMotor.rotate(robot.convertDistance(wheelRadius, travelDis), true);
		rightMotor.rotate(robot.convertDistance(wheelRadius, travelDis), false);
		

		rightMotor.stop(true);
		if(!bool)
		{
			leftMotor.forward();
			try{
				Thread.sleep(85);
			}catch(Exception e){}
			leftMotor.stop(true);
		}
		else
		{
			leftMotor.forward();
			try{
				Thread.sleep(90);
			}catch(Exception e){}
			leftMotor.stop(true);
		}
	
		odometer.setY(y);
		odometer.setX(x);
		odometer.setAng(angle);
	
	}

	/**
	 * TurnTo function which takes an angle and boolean as arguments The boolean
	 * controls whether or not to stop the motors when the turn is completed
	 * @param double turnAngle
	 * @param boolean stop
	 */
	public void turnTo(double turnAngle, boolean stop) {

		// Get the angle the robot needs to turn
		double angleNeedToTravel = (turnAngle - odometer.getTheta()) % 360;

		// Get the minimal angle
		if ((angleNeedToTravel > 180) || (angleNeedToTravel < -180)) {
			if (angleNeedToTravel > 180) {
				angleNeedToTravel = angleNeedToTravel - 360;
			} else {
				angleNeedToTravel = angleNeedToTravel + 360;
			}
		}

		// if the angle is not within the given error margin
		if (Math.abs(angleNeedToTravel) > DEG_ERR) {

			// if angle change is positive we move clockwise
			if (angleNeedToTravel > 0) {

				leftMotor.setSpeed(SLOW);
				rightMotor.setSpeed(SLOW);

				leftMotor.rotate(
						robot.convertAngle(wheelRadius, width,
								Math.abs(angleNeedToTravel)), true);
				rightMotor.rotate(
						-robot.convertAngle(wheelRadius, width,
								Math.abs(angleNeedToTravel)), false);

				// if angle is negative move counterclockwise
			} else {

				leftMotor.setSpeed(SLOW);
				rightMotor.setSpeed(SLOW);

				leftMotor.rotate(
						-robot.convertAngle(wheelRadius, width,
								Math.abs(angleNeedToTravel)), true);
				rightMotor.rotate(
						robot.convertAngle(wheelRadius, width,
								Math.abs(angleNeedToTravel)), false);
			}
		}
		// Stop the motors after we turned the amount we desired
		rightMotor.stop(true);
		leftMotor.stop(true);
		  
	}
	
}
