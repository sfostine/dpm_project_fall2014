/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 27-11-2014
 * 
 * class to pick up and drop off a block
 */
package DPM_NXT_PROJECT;

import lejos.nxt.ColorSensor;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;


public class Pick_Drop {
	//Exlude
	private NXTRegulatedMotor centerMotor, left, right;
	// robot
	private TwoWheeledRobot robot;
	// sensor us
	private UltrasonicSensor us;
	
	ColorSensor sensor;
	int light = 0;

/**
 * 
 * @param UltrasonicSensor usF
 * @param TwoWheeledRobot robot
 * @param lightSensor sen
 */
	public Pick_Drop(UltrasonicSensor usF,TwoWheeledRobot ro, ColorSensor sen)
	{
		centerMotor = Motor.C;
		robot = ro;
		us = usF;
		left = robot.leftMotor;
		right = robot.rightMotor;
		this.sensor = sen;
	}
	
	// move until the us reads a block then pick it up by rottiong the center motor
	public void pick() 
	{
		left.setSpeed(150);
		right.setSpeed(150);
		while(us.getDistance() > 20)
		{
			
			left.forward();
			right.forward();
			if(sensor.getRawLightValue() < 310)
			{
				Sound.beep();
				light += 1;
			}
		}
		
		left.stop();
		right.stop();
		centerMotor.setSpeed(200);
		centerMotor.rotate(-280);
		centerMotor.stop(true);
		
		while(us.getDistance() > 6)
		{
			if(sensor.getRawLightValue() < 310)
			{
				Sound.beep();
				light += 1;
			}
			left.forward();
			right.forward();
			
		}
		left.stop();
		right.stop();
		centerMotor.rotate(360);
		centerMotor.stop(true);
		
		
		
	}
	
	// drop of the block by rotating the center motor
	public void drop()
	{
		centerMotor.setSpeed(200);
		centerMotor.rotate(-310);
		centerMotor.stop(true);
	}
	
	public void run()
	{
		pick();
		
	}
}
