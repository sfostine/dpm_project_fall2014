/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 27-11-2014
 * class that defines the characteristic and data of the robot
 */
package DPM_NXT_PROJECT;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;

public class TwoWheeledRobot {
	public final double DEFAULT_LEFT_RADIUS = 2.17;
	public final double DEFAULT_RIGHT_RADIUS = 2.17;
	public final double DEFAULT_WIDTH = 15.271;
	public NXTRegulatedMotor leftMotor, rightMotor;
	public double leftRadius, rightRadius, width;
	private double forwardSpeed, rotationSpeed;
	
	/**
	 * * Constructor of the class robot
	 * @param leftMotor
	 * @param rightMotor
	 */
	public TwoWheeledRobot(NXTRegulatedMotor leftMotor,
						   NXTRegulatedMotor rightMotor) {
		this.leftMotor = Motor.A;
		this.rightMotor = Motor.B;
		this.leftRadius = DEFAULT_LEFT_RADIUS;
		this.rightRadius = DEFAULT_RIGHT_RADIUS;
		this.width = DEFAULT_WIDTH;
		leftMotor.setAcceleration(1000);
		rightMotor.setAcceleration(1000);
	}
	
	
	/**
	 * get the left radius value
	 * @return the left radius
	 */
	public double getLeftRadius() {
		return DEFAULT_LEFT_RADIUS;
	}

	/**
	 * get the right radius value
	 * @return the right radius
	 */

	public double getRightRadius() {
		return DEFAULT_RIGHT_RADIUS;
	}


	/**
	 * get the width
	 * @return the width between the two motors
	 */
	public double getWidth() {
		return DEFAULT_WIDTH;
	}
	
		
	/**mutators
	 * 
	 * @param double speed
	 */
	public void setForwardSpeed(double speed) {
		forwardSpeed = speed;
		setSpeeds(forwardSpeed, rotationSpeed);
	}
	/**
	 * set the rotational speed of motors
	 * @param double speed
	 */
	public void setRotationSpeed(double speed) {
		rotationSpeed = speed;
		setSpeeds(forwardSpeed, rotationSpeed);
	}
	/**
	 * set the speed of the motors
	 * @param forwardSpeed
	 * @param rotationalSpeed
	 */
	public void setSpeeds(double forwardSpeed, double rotationalSpeed) {
		double leftSpeed, rightSpeed;

		this.forwardSpeed = forwardSpeed;
		this.rotationSpeed = rotationalSpeed; 

		leftSpeed = (forwardSpeed + rotationalSpeed * width * Math.PI / 360.0) *
				180.0 / (leftRadius * Math.PI);
		rightSpeed = (forwardSpeed - rotationalSpeed * width * Math.PI / 360.0) *
				180.0 / (rightRadius * Math.PI);

		// set motor directions
		if (leftSpeed > 0.0)
			leftMotor.forward();
		else {
			leftMotor.backward();
			leftSpeed = -leftSpeed;
		}
		
		if (rightSpeed > 0.0)
			rightMotor.forward();
		else {
			rightMotor.backward();
			rightSpeed = -rightSpeed;
		}
		// set motor speeds
		if (leftSpeed > 900.0)
			leftMotor.setSpeed(900);
		else
			leftMotor.setSpeed((int)leftSpeed);
		
		if (rightSpeed > 900.0)
			rightMotor.setSpeed(900);
		else
			rightMotor.setSpeed((int)rightSpeed);
	}
	
	/** go forward method to make the robot move to a certain distance	
	 * 
	 * @param double distance
	 */
	public void goforward(double distance)
	{
		leftMotor.rotate(convertDistance(leftRadius, distance), true);
		rightMotor.rotate(convertDistance(rightRadius, distance),  false);
		leftMotor.forward();
		try{
			Thread.sleep(110);
		}catch(Exception e){}
		
		leftMotor.stop(true);
	}
	
	/** convert the angle 
	 * 
	 * @param double radius
	 * @param double width
	 * @param double angle
	 * @return the correct angle
	 */
	public int convertAngle(double radius, double width, double angle)
	{
		return convertDistance(radius, Math.PI * width*angle/360.0);
	}
	
	/** convert the distance
	 * 
	 * @param double radius
	 * @param double vect_mag
	 * @return the converted distance
	 */
	public int convertDistance(double radius, double vect_mag)
	{
		return (int) ((vect_mag * 180.0)/ Math.PI * radius);
	}
	
	/** rotate to a specific position
	 * 
	 * @param double angle
	 */
	public void rotate(double angle)
	{
		leftMotor.rotate(convertAngle(leftRadius, width, angle), true);
		rightMotor.rotate(-convertAngle(rightRadius, width, angle), false);	
	}
}
 
