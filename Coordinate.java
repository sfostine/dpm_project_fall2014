/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 29-10-2014
 * 
 * this coordinate class allows the Board class the set the values of x, y and to say if a tile of a specific tile is blocked or not
 * It is implementing to serve the Board class
 */
package DPM_NXT_PROJECT;

public class Coordinate {

	private int x, y;
	private boolean isblocked;
	
	/**
	 * Constructor for Coordinate class
	 * @param X
	 * @param Y
	 * @param block
	 */
	public Coordinate(int X, int Y, boolean block) {
		this.x = X;
		this.y = Y;
		this.isblocked = block;
	}

	/** 
	 * @return x
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * @return y
	 */
	public int getY() {
		return this.y;
	}
	
	/** return if one tile is blocked or not
	 * 
	 * @return boolean isblocked
	 */
	public boolean isblocked()
	{
		return this.isblocked;
	}
}
 
