/**@author Samuel Fostine 
 * Group : 13
 * Date of Modification: 27-11-2014 
 * 
 * This class just put together all the other class to make the robot works
 */
package DPM_NXT_PROJECT;

import lejos.nxt.*;

public class Project_Main {
    int Xi, Xj,Pi,Pj,Di,Dj;

	public static void main(String[] args) {
		int buttonChoice;
        int i = 0;
        int j = 0;
		do {
			LCD.clear();
			// display info to choose a button
			LCD.drawString("< X " + i + "| Y" + j + ">", 0, 0);
			buttonChoice = Button.waitForAnyPress();

		} while (buttonChoice != Button.ID
				&& buttonChoice != Button.ID_RIGHT
				&& buttonChoice != Button.ID_ESCAPE);

		TwoWheeledRobot patBot = new TwoWheeledRobot(Motor.A, Motor.B);
		Odometer odo = new Odometer(patBot, buttonChoice, true);
		Navigation nav = new Navigation(odo, patBot);
		UltrasonicSensor usF = new UltrasonicSensor(SensorPort.S4);
		UltrasonicSensor usL = new UltrasonicSensor(SensorPort.S2);
		UltrasonicSensor usR = new UltrasonicSensor(SensorPort.S3);
		ColorSensor sen = new ColorSensor(SensorPort.S1);
		Map m = null;
		// Pick_Drop p = new Pick_Drop(usF, patBot);
		if (buttonChoice == Button.ID_LEFT) {
			m = new Map(1);
		} else if (buttonChoice == Button.ID_RIGHT) {
			m = new Map(2);
		} else if (buttonChoice == Button.ID_ESCAPE) {
			m = new Map(3);
		}

		Exclude ex = new Exclude(8, m.map, odo, usL, usR, usF, nav, patBot, sen);
		ex.runAlgorithm();

	}
}
