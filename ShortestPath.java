/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 12-11-2014
 * 
 * Shortest path class, return the path from a starting point to a destination point using Bread first seach
 */
package DPM_NXT_PROJECT;

import java.util.Queue;
import java.util.ArrayList;

public class ShortestPath {
	// some initalisations	
	Board b;
	int goalI, goalJ;
	/**
	 * constructor 
	 * @param bo
	 * @param gI
	 * @param gJ
	 */
	public ShortestPath(Board bo, int gI, int gJ)
	{
		this.goalI = gI;
		this.goalJ = gJ;
		this.b = bo;
	}
		/**
		 * return if we get to the goal or not, the goal is the destination
		 * @param current
		 * @return
		 */
	private boolean isGoad(Node current) {
		if(current.getI() == goalI && current.getJ() == goalJ)
			return true;
		return false;
	}
	
	/**
	 * get the adjacent node of a tile and put them inside of an arrayList
	 * @param node
	 * @return arrayList of adjacent nodes
	 */
	/**
	 * caracteristics of adjacent nodes: 1) not blocked 2) not visited 3) not outside of the board
	 */
	private ArrayList<Node> getAdjacentNodes(Node node) {
		// arraylist to contain the adjacent nodes
		ArrayList<Node> nodes = new ArrayList<Node>();
		// y near 0
		if(node.getJ() > 0 && !b.getTile(node.getI(), node.getJ()-1).isblocked() && !node.isVisited(node.getI(), node.getJ()-1, b))
			nodes.add(new Node(node.getI(), node.getJ()-1));
		// y near size
		if(node.getJ() < b.size-1 && !b.getTile(node.getI(), node.getJ()+1).isblocked() && !node.isVisited(node.getI(), node.getJ()+1, b))
			nodes.add(new Node(node.getI(), node.getJ() + 1));
		
		// x near 0
		if(node.getI() > 0 && !b.getTile(node.getI()-1, node.getJ()).isblocked() && !node.isVisited(node.getI() - 1, node.getJ(), b))
			nodes.add(new Node(node.getI()-1, node.getJ()));
		// x near size
		if(node.getI() < b.size-1 && !b.getTile(node.getI() + 1, node.getJ()).isblocked() && !node.isVisited(node.getI() + 1, node.getJ(), b))
			nodes.add(new Node(node.getI()+1, node.getJ()));
		return nodes;
	}
	
	/**
	 * FId the shortest path and return an arrayList of nodes with the path
	 * @param start
	 * @return
	 */
	public ArrayList<Node> FindShortestPath(Node start) {
		
		Node path = null;
		//queue to pop and add element
		Queue <Node>nodesToVisit = new Queue<Node>();
		// set the parent of the start node to null
		start.setParent(null);
		// add the start node to the queue
		nodesToVisit.addElement(start);
		// reset the double array Visited from the board class
		b.resetVisited();
		// while the queue is not empty
		while (!nodesToVisit.isEmpty())
		{
			// pop the node from the queue in store it as current node
			Node current = (Node) nodesToVisit.pop();
			// set the node to be visited
			current.setVisited(current.getI(), current.getJ(), b);
			// check if the node is goal, if yes finish the while loop
			if ( isGoad(current))
			{
				path = current;
				nodesToVisit.clear();
			}
			// if not goal, go to each of the adjacent nodes, set their parent, and add them to the queue
			else
			{
				for (Node adj : getAdjacentNodes(current))
				{	
					
						adj.setParent(current);
						nodesToVisit.addElement(adj);
				}
			}
			
		}
		
		// arraylist to hold the path
		ArrayList<Node> pathList = new ArrayList<Node>();
		
		// ass the path to the arrayList
		while (path.getParent() != null)
		{
			pathList.add(0, path);
			path = path.getParent();
		}
		
		// return the path
		return pathList;
	}
}
