/**@author Samuel Fostine
 * Date of Modification: 29-10-2014 
 * 
 * This class determine the starting position of the robot 
 * by removing all of the arrows that have to be removed depending of the orientation of the robot and what the us reads
 */
package DPM_NXT_PROJECT;
// right one
import lejos.nxt.ColorSensor;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.Color;

public class Exclude {

	/**
	 * some initialization for specific used in the code board
	 */
	private Board b;
	/** the size, NB: the board is size X size */
	private int size;

	/** distance at which it sees a wall or blocked tile */
	private int readWall = 23;

	/**
	 * the starting position will be stored inside of the variables to be
	 * printed
	 */
	public int startingX, startingY;
	public String startingDir;
	public int startI, startJ, currentX, currentY;
	int currentI, currentJ;

	private char currentDir, startD;

	// implement the t array

	private Odometer odo;
	// navigation
	private Navigation nav;
	// robot
	private TwoWheeledRobot robot;
	// sensor us
	private UltrasonicSensor usLeft;
	private UltrasonicSensor usRight;
	private UltrasonicSensor usforward;
	
	private ColorSensor senL;
	/** the arrows that will be excluded at specific tile */
	private Arrow exclu[][];

	// testing arrow for left, right, front
	Arrow excluLeft[][], excluRight[][], excluForward[][];
	/** array list that keeps t if the robot turns left, right or forward */
	private char [] tra = new char[23];
	
	private int msi = 0;

	// degree for turn left method
	private int degree = 0;
	/** the distance it has to run to get to the next tile */
	private double dist = 6.0;

	/**
	 * constructor for the class Exclude
	 * 
	 * @param double array
	 * @param int size
	 * @param Odometer
	 *            od
	 * @param UltrasonicSensor
	 *            sen
	 * @param Navigation
	 *            nav
	 * @param TwoWheeledRobot
	 *            patBot
	 */
	public Exclude(int si, int[][] ar, Odometer od, UltrasonicSensor senLeft,
			UltrasonicSensor senRight, UltrasonicSensor senFor, Navigation n,
			TwoWheeledRobot patBot, ColorSensor s) {
		this.nav = n;
		this.odo = od;
		this.usLeft = senLeft;
		this.usRight = senRight;
		this.usforward = senFor;
		this.robot = patBot;
		this.senL = s;
		this.size = si;
		b = new Board(size, ar);
		exclu = new Arrow[si][si];
		excluLeft = new Arrow[si][si];
		excluRight = new Arrow[si][si];
		excluForward = new Arrow[si][si];

		/** set all of the arrows not to be excluded just to initialize */
		for (int i = 0; i < si; i++) {
			for (int j = 0; j < si; j++) {
				exclu[i][j] = new Arrow(false, false, false, false);
				excluLeft[i][j] = new Arrow(false, false, false, false);
				excluRight[i][j] = new Arrow(false, false, false, false);
				excluForward[i][j] = new Arrow(false, false, false, false);
			}
		}
	}

	/**
	 * first steps, it reads if there's a wall or blocked tile and remove the
	 * arrows that are not facing a whole if there's not wall remove those that
	 * are facing a wall or blocked tile
	 * 
	 * @param distance
	 */
	public void startExclusion(int distance)
    {

		// facing a wall or facing a tile

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (b.tile[i][j].isblocked()) {
					exclu[i][j].setNorth(true);
					exclu[i][j].setSouth(true);
					exclu[i][j].setWest(true);
					exclu[i][j].setEast(true);
				} else if (distance < readWall) {
					if (b.blocked[i][j].isNorth()) {
						exclu[i][j].setNorth(false);
					} else {
						exclu[i][j].setNorth(true);
					}

					if (b.blocked[i][j].isSouth()) {
						exclu[i][j].setSouth(false);
					} else {
						exclu[i][j].setSouth(true);
					}

					if (b.blocked[i][j].isWest()) {
						exclu[i][j].setWest(false);
					} else {
						exclu[i][j].setWest(true);
					}

					if (b.blocked[i][j].isEast()) {
						exclu[i][j].setEast(false);
					} else {
						exclu[i][j].setEast(true);
					}
				}

				// else if not facing a wall or not facing a tile
				else {

					if (b.blocked[i][j].isNorth()) {
						exclu[i][j].setNorth(true);
					} else {
						exclu[i][j].setNorth(false);
					}

					if (b.blocked[i][j].isSouth()) {
						exclu[i][j].setSouth(true);
					} else {
						exclu[i][j].setSouth(false);
					}

					if (b.blocked[i][j].isWest()) {
						exclu[i][j].setWest(true);
					} else {
						exclu[i][j].setWest(false);
					}

					if (b.blocked[i][j].isEast()) {
						exclu[i][j].setEast(true);
					} else {
						exclu[i][j].setEast(false);
					}
				}
			}
		}
	}

	public int testLeftRestArrow(int distLeft) {

		cloneA(excluLeft);
		tra[msi] = 'l';
		
		if (distLeft <= readWall)
			remove_SP(true, excluLeft);
		else
			remove_SP(false, excluLeft);

		return checkRestArrow(excluLeft);
	}

	public int testRightRestArrow(int distRight) {
		
		
		cloneA(excluRight);
		tra[msi] = 'r';
		
		if (distRight <= readWall)
			remove_SP(true, excluRight);
		else
			remove_SP(false, excluRight);

		
		return checkRestArrow(excluRight);
	}

	public int testForwardRestArrow(int distForward) {
		cloneA(excluForward);
		tra[msi] = 'f';
		if (distForward <= (readWall * 2 + 10)) {
			remove_SP(true, excluForward);
		} else {
			remove_SP(false, excluForward);
		}

		return checkRestArrow(excluForward);
	}

	// clone an array of Arrow
	void cloneA(Arrow[][] clone) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (exclu[i][j].isNorth())
					clone[i][j].setNorth(true);
				else
					clone[i][j].setNorth(false);

				if (exclu[i][j].isSouth())
					clone[i][j].setSouth(true);
				else
					clone[i][j].setSouth(false);

				if (exclu[i][j].isWest())
					clone[i][j].setWest(true);
				else
					clone[i][j].setWest(false);

				if (exclu[i][j].isEast())
					clone[i][j].setEast(true);
				else
					clone[i][j].setEast(false);
			}
		}
	}


	/**
	 * turn right by 90 degree
	 * 
	 */
	public void turnRight() {
		degree += 90;
		degree %= 360;
		nav.turnTo(degree, true);
	}

	/**
	 * turn left by 90 degree
	 * 
	 */
	public void turnLeft() {
		degree -= 90;
		degree %= 360;
		nav.turnTo(degree, true);
	}

	/**
	 * move forward by distance to go to the next tile
	 * 
	 */
	public void moveForward() {
		robot.goforward(dist);
		
		if(Math.abs(odo.getTheta() - 90) < 20)
			odo.setAng(90);
		else if(Math.abs(odo.getTheta() - 180) < 20)
			odo.setAng(180);
		else if(Math.abs(odo.getTheta() + 90) < 20)
			odo.setAng(-90);
		
	}

	/**
	 * this method uses a kind of virtual arrow to make it remove_SPel to the
	 * same
	 * 
	 * @param i
	 * @param j
	 * @param dir
	 * @param bl
	 */
	// pattern that the robot remove_SPels to
	public void testremove_SPel(int i, int j, char dir, boolean bl,
			Arrow[][] ar) {
		int x = i;
		int y = j;
		char d = dir;
		
		
		// As array t keeps t of all the movment of the robot, we use
		// those movment to move the virtual arrow

		for(char c : tra) {
	
			// when the robots move forward, we assume the virtual arrow moves
			// forward too, so it was facing north, its y of the virtual arrow
			// will be decreased
			// its direction has its own way to handle the movment of the
			// virtual arrow

			if ((c == 'f')) {
				if (d == 'n') {
					y--;
				} else if (d == 's') {
					y++;
				} else if (d == 'w') {
					x--;
				} else if (d == 'e') {
					x++;
				}
			}

			// when the robot turn left, the virtual arrow also turn left and
			// keep t of its direction and the value of i and j or x and y
			else if (c == 'l') {

				if (d == 'n') {
					d = 'w';
				} else if (d == 's') {
					d = 'e';
				} else if (d == 'w') {
					d = 's';
				} else if (d == 'e') {
					d = 'n';
				}
			}
			// keep t on the right
			else if (c == 'r') {
				if (d == 'n') {
					d = 'e';
				} else if (d == 's') {
					d = 'w';
				} else if (d == 'w') {
					d = 'n';
				} else if (d == 'e') {
					d = 's';
				}
			}
			
		}
		
		// now after making the virtual arrow moves, we check it the robot at
		// its position is blocked or not and make sure that the virtual arrow
		// behaves as the robot
		if ((d == 'n' && bl != b.blocked[x][y].isNorth())
				|| (d == 's' && bl != b.blocked[x][y].isSouth())
				|| (d == 'w' && bl != b.blocked[x][y].isWest())
				|| (d == 'e' && bl != b.blocked[x][y].isEast())) {
			if (dir == 'n')
				ar[i][j].setNorth(true);
			else if (dir == 's')
				ar[i][j].setSouth(true);
			else if (dir == 'w')
				ar[i][j].setWest(true);
			else if (dir == 'e')
				ar[i][j].setEast(true);
		}
		
	}
		

	/**
	 * it return if we still have more than 1 starting position or just 1
	 * starting posititon this method just pass through the whole tiles, check
	 * if the arrows at each tiles are removed or not NB: when a specific arrow
	 * is false, that means it is not removed
	 * 
	 * @return int numberNotRuledOut
	 */

	private int checkRestArrow(Arrow[][] arrow) {
		int t = 0;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (!arrow[i][j].isNorth()) {
					t++;
				}
				if (!arrow[i][j].isSouth()) {
					t++;
				}
				if (!arrow[i][j].isWest()) {
					t++;
				}
				if (!arrow[i][j].isEast()) {
					t++;
				}
			}
		}
		return t;
	}

	/**
	 * remove_SPel to each tile and removed the arrows that are supposed to be
	 * removed NB: this method is just a kind of wrapper method< I could just
	 * put all of those codes inside of the run method
	 * 
	 * @param boolean block
	 */
	public void remove_SP(boolean b, Arrow[][] ar) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {

				if (!ar[i][j].isNorth()) {
					testremove_SPel(i, j, 'n', b, ar);
				}

				if (!ar[i][j].isSouth()) {
					testremove_SPel(i, j, 's', b, ar);
				}
				if (!exclu[i][j].isWest()) {
					testremove_SPel(i, j, 'w', b, ar);
				}
				if (!exclu[i][j].isEast()) {
					testremove_SPel(i, j, 'e', b, ar);
				}
			}
		}
	}

	/**
	 * after all the steps are executed, we have only one arrow left false, that
	 * means it is the starting position this method set the startingX startingY
	 * and startingDir to the right starting position and directionS
	 * 
	 */
	public void discover_SP() {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (!exclu[i][j].isNorthb) {

					startI = i;
					startJ = j;
					startingX = b.getTile(i, j).getX();
					startingY = b.getTile(i, j).getY();
					startingDir = "north";
					startD = 'n';
				}
				if (!exclu[i][j].isSouthb) {

					startI = i;
					startJ = j;
					startingX = b.getTile(i, j).getX();
					startingY = b.getTile(i, j).getY();
					startingDir = "south";
					startD = 's';
				}
				if (!exclu[i][j].isWestb) {

					startI = i;
					startJ = j;
					startingX = b.getTile(i, j).getX();
					startingY = b.getTile(i, j).getY();
					startingDir = "west";
					startD = 'w';
				}
				if (!exclu[i][j].isEastb) {

					startI = i;
					startJ = j;
					startingX = b.getTile(i, j).getX();
					startingY = b.getTile(i, j).getY();
					startingDir = "East";
					startD = 'e';
				}
			}
		}
	}

	/**
	 * get the current position by going from the starting position
	 * 
	 * @param int i
	 * @param int j
	 * @param char dir
	 */
	private void Determ_CurrentPosition(int i, int j, char dir) {
		int x = i;
		int y = j;
		char d = dir;

		// As array t keeps t of all the movment of the robot, we use
		// those movment to move the virtual arrow
		for (char c : tra) {
			// when the robots move forward, we assume the virtual arrow moves
			// forward too, so it was facing north, its y of the virtual arrow
			// will be decreased
			// its direction has its own way to handle the movment of the
			// virtual arrow
			if (c == 'f') {
				if (d == 'n') {
					y--;
				} else if (d == 's') {
					y++;
				} else if (d == 'w') {
					x--;
				} else if (d == 'e') {
					x++;
				}
			}

			// when the robot turn left, the virtual arrow also turn left and
			// keep t of its direction and the value of i and j or x and y
			else if (c == 'l') {

				if (d == 'n') {
					d = 'w';
				} else if (d == 's') {
					d = 'e';
				} else if (d == 'w') {
					d = 's';
				} else if (d == 'e') {
					d = 'n';
				}
			} else if (c == 'r') {
				if (d == 'n') {
					d = 'e';
				} else if (d == 's') {
					d = 'w';
				} else if (d == 'w') {
					d = 'n';
				} else if (d == 'e') {
					d = 's';
				}
			}
		}
		currentI = x;
		currentJ = y;
		currentX = b.getTile(x, y).getX();
		currentY = b.getTile(x, y).getY();
		currentDir = d;
	}

	/**
	 * reset degree to 0
	 * 
	 */
	public void resetDeg() {
		this.degree = 0;
	}

	/**
	 * travel to the pick up and drop off area
	 * @param curI
	 * @param curJ
	 * @param i
	 * @param j
	 */
	public void travelToPick_Drop(int curI, int curJ, int i, int j) {
		ShortestPath pathP = new ShortestPath(b, i, j);
		Node n = new Node(curI, curJ);
		for (Node n1 : pathP.FindShortestPath(n)){
			nav.travelTo(b.getTile(n1.getI(), n1.getJ()).getX(), b.getTile(n1.getI(), n1.getJ()).getY(), true);
			odo.setX(b.getTile(n1.getI(), n1.getJ()).getX());
			odo.setY(b.getTile(n1.getI(), n1.getJ()).getY());
			if(Math.abs(odo.getTheta() - 90) < 20)
				odo.setAng(90);
			else if(Math.abs(odo.getTheta() - 180) < 20)
				odo.setAng(180);
			else if(Math.abs(odo.getTheta() + 90) < 20)
				odo.setAng(-90);
		}
	}


	/**
	 * The wrapper method that will be called to run the algorithm
	 * 
	 */
	public void runAlgorithm() {
		int left = 0;
		int right = 0;
		int forward = 0;
		int curDist = usforward.getDistance();
		
		int distLeft = 0;
		int distRight = 0;
		int distFor = 0;
		// it first takes the distance of the robot from its starting position
		// and remove the arrows that are facing a wall when the robot started
		// by not faing a wall
		// removes those that are not facing a whole when the robot started by
		// facing a whole
		
		startExclusion(curDist);
		// while the remaining arrow is not one, keep going
		while (checkRestArrow(exclu) != 1) 
		{
			curDist = usforward.getDistance();
			distLeft = usLeft.getDistance();
			distRight = usRight.getDistance();
			distFor = curDist;
			if (curDist <= readWall) {
				
				right = testRightRestArrow(distRight);
				left = testLeftRestArrow(distLeft);

				if (left <= right) 
				{
					turnLeft();
					tra[msi] = 'l';
					msi++;
					if (distLeft <= readWall) {
						remove_SP(true, exclu);
					} else {
						remove_SP(false, exclu);
					}
				} else if (right < left) {
					turnRight();
					tra[msi] = 'r';
					msi++;
					if (distRight <= readWall)
						remove_SP(true, exclu);
					else
						remove_SP(false, exclu);
				}
			} 
			
			else if (curDist > readWall) 
			{
				right = testRightRestArrow(distRight);
				left = testLeftRestArrow(distLeft);
				forward = testForwardRestArrow(distFor);

				if (left <= right && left < forward) {
					turnLeft();
					tra[msi] = 'l';
					msi++;
					if (distLeft <= readWall)
					{
						remove_SP(true, exclu);
					}
					else
					{
						remove_SP(false, exclu);
					}
				} 
				else if (right <= left && right < forward) {
					turnRight();
					tra[msi] = 'r';
					msi++;
					if (distRight <= readWall)
						remove_SP(true, exclu);
					else
						remove_SP(false, exclu);
				} 
				else 
				{
					moveForward();
					
					//tra[msi] = 'f';
					msi += 1;
					
					if (distFor <= (readWall*2+10))
						remove_SP(true, exclu);
					else
						remove_SP(false, exclu);
				}

			}
			
		}
		// sound when fiding position
		Sound.twoBeeps();
		// close the us that are not useful anymore
		usLeft.off();
		usRight.off();
		// call discover starting position
		discover_SP();
		// determine the current position
		Determ_CurrentPosition(startI, startJ, startD);
		// set the value of the odometer to be the same as the current position
		setOd();
		// travel to the pick up area
		travelToPick_Drop(currentI, currentJ,0, 2);
		//turn to south
		nav.turnTo(180, true);
		
		// set the light sensor
		senL.setFloodlight(true);
		senL.setFloodlight(Color.BLUE);
		
		// pich the block
		Pick_Drop p = new Pick_Drop(usforward, robot, senL);
		p.run();
		//go back the the position left by the shortest path algorithm
		nav.turnTo(0, true);
		robot.leftMotor.forward();
		robot.rightMotor.forward();
		while(true)
		{
			if(senL.getRawLightValue() < 290)
			{
				p.light -= 1;
				Sound.beep();
				if(p.light <= 0)
				{
					robot.leftMotor.stop(true);
					robot.rightMotor.stop(true);
					break;
				}
			}
			
		}
		
		robot.goforward(3.8);
		odo.setX(b.getTile(0, 2).getX());
		odo.setY(b.getTile(0, 2).getY());
		// reset the boolean visited fron the board class
		b.resetVisited();
		// travel to the dropoff area
		travelToPick_Drop(0, 2, 3, 3);
		robot.leftMotor.backward();robot.rightMotor.backward();
		try{Thread.sleep(500);}catch(Exception e){}
		// drop the block
		p.drop();
		
	}
	/**
	 * set the values of the odometer to be the va;ue of the current position and orientation
	 */
	void setOd()
	{
		odo.setX(currentX);
		odo.setY(currentY);
		if(currentDir == 'n')
		{
			odo.setAng(0);
		}
		else if(currentDir == 's')
		{
			odo.setAng(180);
		}
		else if(currentDir == 'w')
		{
			odo.setAng(-90);
		}
		else if(currentDir == 'e')
		{
			odo.setAng(90);
		}
	}
}
