/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 29-10-2014
 * Define the direction of all arrows (North, South, West, East) that will be inside of each tile
 * it will be used by Board class and ExcludeArrow class to define to know if a tile is blocked from N, S, W, E
 * In ExcludeArrow class, to know if a certain direction is ruled out or not
 */
package DPM_NXT_PROJECT;

public class Arrow {
	/**
	 *  Boolean to define the direction (blocked or not, ruled out or not)
	 */
	boolean isNorthb, isSouthb, isWestb, isEastb;
	
	/**
	 * Constructor for Arrow class
	 * @param northb
	 * @param southb
	 * @param westb
	 * @param eastb
	 */
	public Arrow(boolean northb, boolean southb, boolean westb, boolean eastb ) {
		this.isNorthb = northb;
		this.isSouthb = southb;
		this.isWestb = westb;
		this.isEastb = eastb;
	}

	/**
	 * 
	 * @return boolean for the north
	 */
	public boolean isNorth()
	{
		return this.isNorthb;
	}
	
	/**
	 * 
	 * @return boolean for the south
	 */
	public boolean isSouth()
	{
		return this.isSouthb;
	}

	/**
	 * 
	 * @return boolean for the West
	 */
	public boolean isWest()
	{
		return this.isWestb;
	}
	
	/**
	 * 
	 * @return boolean for the East
	 */
	public boolean isEast()
	{
		return this.isEastb;
	}
	
	/**
	 * set North blocked or not, ruled out or not
	 * @param boolean block
	 */
	public void setNorth(boolean block)
	{
		this.isNorthb = block;
	}
	
	/**
	 * set South blocked or not, ruled out or not
	 * @param boolean block
	 */
	public void setSouth(boolean block)
	{
		this.isSouthb = block;
	}
	
	/**
	 * set West blocked or not, ruled out or not
	 * @param boolean block
	 */
	public void setWest(boolean block)
	{
		this.isWestb = block;
	}
	/**
	 * set East blocked or not, ruled out or not
	 * @param boolean block
	 */
	public void setEast(boolean block)
	{
		this.isEastb = block;
	}
}
 
