/**
 * @author Samuel Fostine 
 * Group : 13
 * Date of modification : 12-11-2014
 * 
 * Node class to find the shortest path
 */
package DPM_NXT_PROJECT;

public class Node {

		private int i, j;	
		private Node parent;
		
	/**
	 * Constructor of node	
	 * @param i
	 * @param j
	 */
	public Node(int i, int j) {
		this.i = i;
		this.j = j;
		this.parent = null;
	}

	/**
	 * getter method to know if a know is visited or not
	 * @param i
	 * @param j
	 * @param b
	 * @return
	 */
	public boolean isVisited(int i, int j,Board b) {
		return b.visited[i][j];
	}

	/**
	 * set the node to be visited
	 * @param i
	 * @param j
	 * @param b
	 */
	public void setVisited(int i, int j,Board b) {
		b.visited[i][j] = true;
	}

	/**
	 * get the parent of a node
	 * @return parent
	 */
	public Node getParent() {
		return parent;
	}

	/**
	 * set the parent of a node
	 * @param parent
	 */
	public void setParent(Node par) {
		this.parent = par;
	}
	
	/**
	 * get the value of index i
	 * @return i
	 */
	public int getI() {
		return i;
	}
	/**
	 * set the value of index i
	 * @param i
	 */
	public void setI(int i) {
		this.i = i;
	}

	/**
	 * get the value of index j
	 * @return j
	 */
	public int getJ() {
		return j;
	}

	/**
	 * set the value of index j
	 * @param j
	 */
	public void setJ(int j) {
		this.j = j;
	}
}
